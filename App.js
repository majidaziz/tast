/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { Home } from './Component/Screen/HomeScreen';
import {createStore,applyMiddleware} from 'redux';
import {Provider} from 'react-redux'
import Combine from './Redux/Reducer/index'
import { NavigationContainer,  } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import thunk from 'redux-thunk';
import {DetailsScreen} from './Component/Screen/DetailsScreen';

const Stack = createStackNavigator();
const App: () => React$Node = () => {
  const Store=createStore(Combine,applyMiddleware(thunk))
  return (
    <Provider store={Store}>
    <>
      <StatusBar barStyle='light-content'  hidden = {true}  translucent = {true}/>
      <SafeAreaView style={{flex:1}}>
      {/* <DetailsScreen/> */}
      <NavigationContainer  >
      <Stack.Navigator   initialRouteName={"Home"} screenOptions={{headerTintColor:'black', headerBackTitle:' ',  }}>
      <Stack.Screen name="Home" options={{title: null,headerShown: false,}} component={Home}  />
      
      <Stack.Screen name="Detail" options={{title: null,headerShown: false,}} component={DetailsScreen}  />
      </Stack.Navigator>
      </NavigationContainer>
       </SafeAreaView>
    </>
    </Provider>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
