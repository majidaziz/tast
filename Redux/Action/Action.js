import { USER_SIGNOUT,GET } from "../Type/type";

  export const postData = () => {
    try{
      dispatch({ type: USER_SIGNOUT })
      return true
    }
    catch (e) {
      return false;
    }
  };
  
  // export const   postData ;
  
  export const getData = (res) => {
    try{
      dispatch({ type: GET,payload:res})
      return true
    }
    catch (e) {
      return false;
    }
  };