import React from 'react'
import {
    View,
    Text
} from 'react-native'
import Textarea from 'react-native-textarea';

export const TextArea = (props) => {
    const {
        containerStyle
    } = styles;
    return (
    <View style={styles.container}><Text
    style={
        styles.labelStyle
    }>
    {
        props.label
    }
</Text>
        <Textarea
          containerStyle={styles.textareaContainer}
          style={styles.textarea}
          value={props.value}
          onChangeText={(text)=>props.onChangeText(text)}
          maxLength={120}
          placeholder={'Summery'}
          placeholderTextColor={'#c7c7c7'}
          underlineColorAndroid={'transparent'}
        />
      </View>
    )
}
const styles = {
    container: {
        flex: 1,
      },
      textareaContainer: {
        height: '80%',
        padding: 5,
        backgroundColor: '#F5FCFF',
      },
      textarea: {
        textAlignVertical: 'top',  // hack android
        height: 170,
        fontSize: 14,
        color: '#333',
      }, labelStyle: {
        fontSize: 18,
        paddingLeft: 20,
        flex: 1
    },
}