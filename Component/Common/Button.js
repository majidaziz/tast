import React from 'react'
import {
    Text,
    TouchableOpacity
} from 'react-native'

const Button = ({ onPress, children }) => {
    const {
        textStyle,
        buttonStyle
    } = styles;
    return (
        <TouchableOpacity
            onPress={onPress}
            style={
                buttonStyle
            }>
            <Text
                style={
                    textStyle
                }>
                {
                    children
                }
            </Text>
        </TouchableOpacity>
    )
}
const styles = {
    textStyle: {
        alignSelf: 'center',
        paddingTop: 10,
        paddingBottom: 10,
        fontSize: 16,
        fontWeight: '600',
        color: '#007aff',
    },
    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        borderColor: '#007aff',
        borderRedius: 5,
        borderWidth: 1,
        backgroundColor: '#ddd',
        marginHorizontal: 5
    }
}
export { Button };