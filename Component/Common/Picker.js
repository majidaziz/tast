import React, { useState } from "react";
import { View,  StyleSheet,Text } from "react-native";
import {Picker} from '@react-native-picker/picker';

export const DropDown = (props) => {
  const [selectedValue, setSelectedValue] = useState("java");
  return (
    <View style={styles.container}>
    <View style={{flexDirection:'row',justifyContent:'space-between'}}>
    <Text
                style={
                    styles.labelStyle
                }>
                {
                    props.label
                }
            </Text>
      <Picker
        selectedValue={props.selectedValue}
        mode='dropdown'
        style={{ height: 50, width: '70%' ,bottom:'3%'}}
        onValueChange={(item)=>props.onValueChange(item)}
      >
        <Picker.Item label="Java" value="java" />
        <Picker.Item label="JavaScript" value="js" />
      </Picker>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
      flex:1,
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
  }, labelStyle: {
        fontSize: 18,
        paddingLeft: 20,
        flex: 1,
    },
});

