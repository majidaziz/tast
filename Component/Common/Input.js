import React from 'react'
import {
    Text,
    TextInput,
    View
} from 'react-native'

export const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => {
    const {
        containerStyle,
        labelStyle,
        TextinputStyle
    } = styles;
    return (
        <View
            style={
                containerStyle
            }>
            <Text
                style={
                    labelStyle
                }>
                {
                    label
                }
            </Text>
            <TextInput
                secureTextEntry={
                    secureTextEntry
                }
                placeholder={
                    placeholder
                }
                value={
                    value
                }
                onChangeText={
                    onChangeText
                }
                style={
                    TextinputStyle
                }
            />
        </View>
    )
}
const styles = {
    labelStyle: {
        fontSize: 18,
        paddingLeft: 20,
        flex: 1,
    },
    TextinputStyle: {
        paddingLeft: 5,
        paddingRight: 5,
        flex: 2,
        lineHeight: 23,
        color: '#000',
    },
    containerStyle: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
    },
}