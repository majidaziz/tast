import React, { useState } from "react";
import { View,  StyleSheet,Text } from "react-native";
import CheckBox from '@react-native-community/checkbox';

export const Checkbox = (props) => {
    const [toggleCheckBox, setToggleCheckBox] = useState(false)
  return (
    <View style={styles.container}>
     <Text
                style={
                    styles.labelStyle
                }>
                {
                    props.label
                }
            </Text>

            
            <CheckBox
    disabled={false}
    value={props.value}
    onValueChange={(newValue) => props.onValueChange(newValue)}
  />
    <Text
                style={[
                    styles.labelStyle
                ,{marginLeft:'40%'}]}>
                {
                    props.labelName
                }
            </Text>
    
     
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
      flexDirection: 'row',
      alignItems: 'center',
      height: 40,
    }, labelStyle: {
          fontSize: 18,
          paddingLeft: 20,
          flex: 1
      },
});

