import React from 'react'
import {
    View,
    Text
} from 'react-native'

export const Card = (props) => {
    const {
        containerStyle
    } = styles;
    return (
        <View
            style={
                containerStyle
            }>
            {
                props.children
            }

        </View>
    )
}
const styles = {
    containerStyle: {
        borderWidth: 1,
        borderRedius: 2,
        borderColor: '#ddd',
        borderBottomRedius: 0,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.1,
        shadowRedius: 2,
        elevation: 1,
        marginTop: 10,
        marginHorizontal: 15,flex:1
    }
}