import React, { useState } from "react";
import { View,  StyleSheet,Text } from "react-native";
import CheckBox from '@react-native-community/checkbox';

export const ErrorMsg = (props) => {
    const [toggleCheckBox, setToggleCheckBox] = useState(false)
  return (
     <Text
                style={
                    styles.labelStyle
                }>
                {
                    props.label
                }
            </Text>

        
  );
}

const styles = StyleSheet.create({
    container: {
        flex:1,
      flexDirection: 'row',
      alignItems: 'center',
    }, labelStyle: {
          fontSize: 18,
          paddingLeft: 20,
          flex: 1,color:'red'
      },
});

