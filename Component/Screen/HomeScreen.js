import React,{useState} from 'react';
import {View,Text,SafeAreaView,StatusBar,StyleSheet} from 'react-native'
import { Header } from '../Common/Header';
import {Input} from '../Common/Input'
import {Button} from '../Common/Button'
import {Card} from '../Common/Card'
import { DropDown } from '../Common/Picker';
import { TextArea } from '../Common/Textarea';
import { Checkbox } from '../Common/Checkbox';
import {postData} from '../../Redux/Action/Action'

import {useSelector, useDispatch} from 'react-redux';
import { ErrorMsg } from '../Common/ErrorMsg';
import { Spinner } from '../Common/Spinner';
export const Home=({navigation})=>{
    const dispatch=useDispatch()
    const User = useSelector(state => ({...state}));
    const [check,setCheck]=useState(false)
    const [_username, onChangeText_u] = useState(''); 
    const [textarea, gettextarea] = useState('');   
    const [selectedValue, setSelectedValue] = useState("java");
    const [toggleCheckBox, setToggleCheckBox] = useState(false)
    console.log(User)
    const submit=async()=>{
        setCheck(true)
        if(_username!=""&&textarea!=""){
      console.log("fail")
        var myHeaders = new Headers();
myHeaders.append("Content-Type", "application/json");

var raw = JSON.stringify({"name":" Aziz","currency":"usd","token":"tok_visa"});

var requestOptions = {
  method: 'POST',
  headers: myHeaders,
  body: raw,
  redirect: 'follow'
};

await fetch("https://jsonplaceholder.typicode.com/posts", requestOptions)
  .then(response => response.text())
  .then(result => {
    console.log("failed",result)
      const data=true
      setCheck(false)
      navigation.navigate('Detail')
      dispatch(postData)})
  .catch(error => {
      
      console.log("fail333",error)
    const data=false
    setCheck(false)
    dispatch(postData)});
   } }
    return(
        <View style={{flex:1,}}>
           <Header headerText={'Tech Carrot Task'}/>
           
            <Text style={{textAlign:'center',fontSize:24}}>Please Fill the Form</Text>
            
            <Card>
           <Input label={'Name : '} 
               placeholder={"Text"}
               value={_username}
               onChangeText={(text)=>onChangeText_u(text)}
           />
           {
               check&&_username===''&&<ErrorMsg label={"Name must be required."}/>
           }
           <DropDown label={'language : '}
               selectedValue={selectedValue}
               onValueChange={(itemValue, itemIndex) => setSelectedValue(itemValue)}
           />
           <Checkbox label={"Gender : "} labelName={"Male"} value={toggleCheckBox}
            onValueChange={(newValue) => setToggleCheckBox(newValue)}

           />
           <TextArea label={"Summery : "} 
           value={textarea}
          onChangeText={(text)=>gettextarea(text)}/>
           {
               check&&textarea===''&&<ErrorMsg label={"Summery must be required."}/>
           }
           </Card>
           <View style={{height:60,marginHorizontal:20,marginVertical:30}}>
           <Button onPress={submit}>
             {  check?<Spinner/> :<Text>Submit</Text>}
           </Button>
           </View>
        </View>
    )
}