import React,{useState,useEffect} from 'react'
import {
    Text,
    TextInput,
    FlatList,
    StyleSheet,
    View,TouchableOpacity,
    Alert
} from 'react-native'
import { Card } from '../Common/Card';
import { Header } from '../Common/Header';
import {useSelector, useDispatch} from 'react-redux';
import { Spinner } from '../Common/Spinner';
import { getData } from '../../Redux/Action/Action';

export const DetailsScreen =({navigation})=>{
const [data,useData]=useState([])
const [dataLoading,useDataLoading]=useState(false)
const dispatch=useDispatch()
useEffect(()=>{
    Done()
  },[])
 const Done=async()=>{
    var myHeaders = new Headers();
    
    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow'
    };
    
    await fetch("https://jsonplaceholder.typicode.com/posts", requestOptions)
      .then(response => response.json())
      .then(result => {
          useData(result)
          useDataLoading(true)
dispatch({type: 'get', payload: result})
      })
      .catch(error => console.log('error', error));
  }
        return(

            <View style={[styles.container,{justifyContent:!dataLoading?'center':'flex-start',alignItem:!dataLoading?'center':'flex-start'}]}>  
  
            <Header headerText={'Tech Carrot Task'}/>
           
           <Text style={{textAlign:'center',fontSize:24}}>Data Of Posts</Text>
           {
                !dataLoading&&<Spinner/>
            }
            <FlatList 
            keyExtractor = { (item, index) => index.toString() } 
            data={data}  
            renderItem={({item}) =>  
               
               <Card >
               <TouchableOpacity
              >
                    <Text style={styles.title} >{item.title}</Text>
                    <Text style={styles.body}>{item.body}</Text>
             </TouchableOpacity>
                </Card>
             }  
            />  
            </View>
        )
        };


const styles = StyleSheet.create({  
    container: {  
        flex: 1,  
    },  
   title:{
       fontSize:18,
       fontWeight:'bold',
       padding:10
   } ,
   body:{
    fontSize:12,
    fontWeight:'bold',
    padding:10
}
})  