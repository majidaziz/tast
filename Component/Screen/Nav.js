
import React from 'react';
import { NavigationContainer,  } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import thunk from 'redux-thunk';
import DetailsScreen from './DetailsScreen';
import Home from './HomeScreen';
const Stack = createStackNavigator();
export const Nav=()=>{
    return(
        <NavigationContainer  >
      <Stack.Screen name="Home" options={{title: null,}} component={Home}  />
      
      <Stack.Screen name="Detail" options={{title: null,}} component={DetailsScreen}  />
      </NavigationContainer>
    )
}